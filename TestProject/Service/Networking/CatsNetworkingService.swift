//
//  CatsNetworkingService.swift
//  TestProject
//
//  Created by Nikita Elizarov on 30.06.2020.
//

import Moya

protocol ICatsNetworkingService {
    var catsAPIProvider: MoyaProvider<CatsAPI> { get }
    
    func loadCats(limit: Int, page: Int, completion: @escaping (Result<Response, MoyaError>) -> Void)
}

struct CatsNetworkingService: ICatsNetworkingService {
    var catsAPIProvider = MoyaProvider<CatsAPI>(plugins: [NetworkLoggerPlugin()])
        
    func loadCats(limit: Int, page: Int, completion: @escaping (Result<Response, MoyaError>) -> Void) {
        catsAPIProvider.request(.getAllCats(limit, page)) { result in
           completion(result)
        }
    }
}
