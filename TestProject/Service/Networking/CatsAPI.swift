//
//  CatsAPI.swift
//  TestProject
//
//  Created by Nikita Elizarov on 30.06.2020.
//

import Moya

enum CatsAPI {
    case getAllCats(Int, Int)
}

extension CatsAPI: TargetType {
    public var baseURL: URL {
        return URL(string: "https://api.thecatapi.com/v1/images")!
    }
        
    public var path: String {
        switch self {
        case .getAllCats:
            return "/search"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .getAllCats:
            return .get
        }
    }
    
    public var task: Task {
        switch self {
        case .getAllCats(let limit, let page):
            let parameters: [String: Int] = ["limit" : limit, "page" : page]
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        }
    }
    public var sampleData: Data {
        switch self {
        case .getAllCats:
            return Data()
        }
    }

    public var headers: [String: String]? {
        return nil
    }
}

