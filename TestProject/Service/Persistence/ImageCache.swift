//
//  ImageCache.swift
//  TestProject
//
//  Created by Nikita Elizarov on 01.07.2020.
//

import UIKit

let imageCache = NSCache<NSString, UIImage>()

final class ImageCache {
    
    class func getCachedImage(by url: URL, with placeholder: UIImage?, completion: @escaping (UIImage) -> Void) {
        
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
            completion(cachedImage)
            return
        } else {
            if let placeholderImage = placeholder {
                completion(placeholderImage)
            }
            let task = URLSession.shared.dataTask(with: url) { imageData, response, error in
                guard let imageData = imageData, error == nil, let image = UIImage(data: imageData) else { return }
                imageCache.setObject(image, forKey: url.absoluteString as NSString)
                DispatchQueue.main.async {
                    completion(image)
                }
            }
            
            task.resume()
        }
        
    }
}

extension UIImageView {
    func cachedImage(for url: URL, with placeholder: UIImage?){
                
        if let imageFromCache = imageCache.object(forKey: url.absoluteString as NSString) {
            self.image = imageFromCache
            return
        }
        
        image = placeholder
        
        URLSession.shared.dataTask(with: url) {
            data, response, error in
            if let response = data, let imageToCache = UIImage(data: response) {
                imageCache.setObject(imageToCache, forKey: url.absoluteString as NSString)
                DispatchQueue.main.async {[weak self] in
                    self?.image = imageToCache
                }
            }
        }.resume()
    }
}
