//
//  CellConfigurator.swift
//  TestProject
//
//  Created by Nikita Elizarov on 30.06.2020.
//

import UIKit

protocol ConfigurableCell {
    associatedtype DataType
    
    var onAction: (() -> Void)? { get set }
    
    func configure(data: DataType)
}

protocol CellConfigurator {
    static var reuseId: String { get }
    var onAction: (() -> Void)? { get set }
    func configure(cell: UIView)
}

class TableCellConfigurator<CellType: ConfigurableCell, DataType>: CellConfigurator where CellType.DataType == DataType, CellType: UITableViewCell {
    static var reuseId: String { return String(describing: CellType.self) }
    
    var item: DataType
    var onAction: (() -> Void)?

    init(item: DataType, onAction: (() -> Void)? = nil) {
        self.item = item
        self.onAction = onAction
    }

    func configure(cell: UIView) {
        var cell = cell as! CellType
        cell.configure(data: item)
        cell.onAction = onAction
    }
}


