//
//  UIViewController+fromNib.swift
//  TestProject
//
//  Created by Nikita Elizarov on 30.06.2020.
//

import UIKit

extension UIViewController {
    static func instantiateFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>(_ viewType: T.Type) -> T {
            return T.init(nibName: String(describing: T.self), bundle: nil)
        }
        
        return instantiateFromNib(self)
    }
}
