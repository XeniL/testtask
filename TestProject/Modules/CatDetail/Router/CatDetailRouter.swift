//
//  CatDetailRouter.swift
//  CIViperGenerator
//
//  Created by Nikita Elizarov on 01.07.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import Foundation
import UIKit

protocol CatDetailRouterInterface: class {

}

class CatDetailRouter: NSObject {

    weak var viewController: CatDetailViewController?

    static func setupModule(catInfo: Cat) -> CatDetailViewController {
        let vc = CatDetailViewController.instantiateFromNib()

        let interactor = CatDetailInteractor()
        let router = CatDetailRouter()
        let presenter = CatDetailPresenter(interactor: interactor, router: router, view: vc)
        presenter.catInfo = catInfo

        vc.presenter = presenter
        vc.router = router
        router.viewController = vc
        interactor.presenter = presenter
        return vc
    }
}

extension CatDetailRouter: CatDetailRouterInterface {

}

