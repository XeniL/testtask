//
//  CatDetailInteractor.swift
//  CIViperGenerator
//
//  Created by Nikita Elizarov on 01.07.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import Foundation

protocol CatDetailInteractorInterface: class {

}

class CatDetailInteractor {
    weak var presenter: CatDetailPresenterInterface?
}

extension CatDetailInteractor: CatDetailInteractorInterface {

}
