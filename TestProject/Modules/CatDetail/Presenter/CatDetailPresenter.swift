//
//  CatDetailPresenter.swift
//  CIViperGenerator
//
//  Created by Nikita Elizarov on 01.07.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import Foundation
import UIKit

protocol CatDetailPresenterInterface: class {
    func viewIsReady()
}

class CatDetailPresenter {

    unowned var view: CatDetailViewControllerInterface
    let router: CatDetailRouterInterface?
    let interactor: CatDetailInteractorInterface?
    
    var catInfo: Cat!

    init(interactor: CatDetailInteractorInterface, router: CatDetailRouterInterface, view: CatDetailViewControllerInterface) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    private func prepareCatDetail() {
        guard let url = URL(string: catInfo.url) else { return }
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        
        let json = try? encoder.encode(catInfo)
        let stringRepresentable = String(json?.prettyPrintedJSONString ?? "")
        view.setTestView(text: stringRepresentable)
        
        ImageCache.getCachedImage(by: url, with: UIImage(named: "cat_placeholder")) {[weak self] image in
            self?.view.setImageView(image: image)
        }
        
//        Cache.getImageByURL(url: url) {[weak self] image, _ in
//            DispatchQueue.main.async {
//                self?.view.setImageView(image: image)
//            }
//        }
    }
}

extension CatDetailPresenter: CatDetailPresenterInterface {
    func viewIsReady() {
        prepareCatDetail()
    }
}
