//
//  CatDetailViewController.swift
//  CIViperGenerator
//
//  Created by Nikita Elizarov on 01.07.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

protocol CatDetailViewControllerInterface: class {
    func setImageView(image: UIImage)
    func setTestView(text: String)
}

class CatDetailViewController: UIViewController {
    @IBOutlet weak var catImageView: UIImageView!
    @IBOutlet weak var catJSONRepresentableTextView: UITextView!
    
    var presenter: CatDetailPresenterInterface?
    var router: CatDetailRouterInterface?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationView()
        presenter?.viewIsReady()
    }
    
    private func configureNavigationView() {
        title = "Cat"
        navigationItem.largeTitleDisplayMode = .never
    }
    
}

extension CatDetailViewController: CatDetailViewControllerInterface {
    
    func setImageView(image: UIImage) {
        catImageView.image = image
    }
    func setTestView(text: String) {
        catJSONRepresentableTextView.text = text
    }
}
