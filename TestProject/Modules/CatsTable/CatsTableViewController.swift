//
//  CatsTableViewController.swift
//  CIViperGenerator
//
//  Created by Nikita Elizarov on 30.06.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

protocol CatsTableViewControllerInterface: class {
    func reloadTableView()
    func animateLoading(state: Bool)
    func openDetails(catInfo: Cat)
    func insertRows(at indexPaths: [IndexPath])
    func tableViewBeginUpdates()
    func tableViewEndUpdates()
}

class CatsTableViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: CatsTablePresenterInterface!
    var router: CatsTableRouterInterface!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationView()
        configureTableView()
        presenter.viewIsReady()
    }
    
    private func configureNavigationView() {
        title = "Cats"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
    }
    
    private func configureTableView() {
        tableView.register(UINib(nibName: "CatImageTableViewCell", bundle: nil), forCellReuseIdentifier: "CatImageTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        presenter.configure(tableView: tableView)
    }
}

extension CatsTableViewController: CatsTableViewControllerInterface {
    func reloadTableView() {
        tableView.reloadData()
    }
    
    func insertRows(at indexPaths: [IndexPath]) {
        tableView.insertRows(at: indexPaths, with: .fade)
    }
    
    func tableViewBeginUpdates() {
        tableView.beginUpdates()
    }
    
    func tableViewEndUpdates() {
        tableView.endUpdates()
    }
    
    func openDetails(catInfo: Cat) {
        router.openCatDetails(catInfo: catInfo)
    }
    
    func animateLoading(state: Bool) {
        state ? view.showAnimatedGradientSkeleton() : view.hideSkeleton()
    }
}
