//
//  CatImageTableViewCell.swift
//  TestProject
//
//  Created by Nikita Elizarov on 30.06.2020.
//

import UIKit
import SkeletonView

typealias CatImageTableViewConfigurator = TableCellConfigurator<CatImageTableViewCell, CatImageModel>

class CatImageTableViewCell: UITableViewCell, ConfigurableCell {
  
    @IBOutlet private weak var catImage: UIImageView!
    @IBOutlet private weak var catImageDescription: UILabel!

    var onAction: (() -> Void)?
    
    private var id: String = ""

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        catImage.image = nil
        catImageDescription.text = nil
    }
    
    func configure(data: CatImageModel) {
        catImageDescription.text = data.imageSize
        id = data.id
        
        guard let url = URL(string: data.imageURL) else { return }
        
        ImageCache.getCachedImage(by: url, with: UIImage(named: "cat_placeholder")) {[weak self] image in
            guard self?.id == data.id else { return }
            self?.catImage.image = image
        }
    }
    
    func startAnimations(_ start: Bool) {
        [catImage, catImageDescription].forEach {
            start ? $0.showAnimatedGradientSkeleton() : $0.hideSkeleton()
        }
    }
    
}
