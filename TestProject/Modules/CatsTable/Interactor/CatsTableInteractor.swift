//
//  CatsTableInteractor.swift
//  CIViperGenerator
//
//  Created by Nikita Elizarov on 30.06.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import Moya

protocol CatsTableInteractorInterface {
    func fetchCats(limit: Int, page: Int)
}

protocol CatsTableInteractorOutput: class {
    func onCatsLoaded(result: Result<[Cat], Error>)
}

class CatsTableInteractor {
    weak var presenter: CatsTableInteractorOutput?
    var catsNetworkingService: ICatsNetworkingService!
}

extension CatsTableInteractor: CatsTableInteractorInterface {
    func fetchCats(limit: Int, page: Int) {
        catsNetworkingService.loadCats(limit: limit, page: page) { result in
            switch result {
            case .success(let response):
                do {
                    let filteredResponse = try response.filterSuccessfulStatusCodes()
                    let cats = try filteredResponse.map([Cat].self)
                    
                    self.presenter?.onCatsLoaded(result: .success(cats))
                } catch let error {
                    self.presenter?.onCatsLoaded(result: .failure(error))
                }
            case .failure(let moyaError):
                self.presenter?.onCatsLoaded(result: .failure(moyaError))
            }
        }
    }
}
