//
//  CatsTablePresenter.swift
//  CIViperGenerator
//
//  Created by Nikita Elizarov on 30.06.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import UIKit

protocol CatsTablePresenterInterface: class {
    func configure(tableView: UITableView)
    func viewIsReady()
}

class CatsTablePresenter: NSObject {

    private struct Constants {
        static let itemsLimit = 20
        static let contentOffset: CGFloat = 200
    }
    
    unowned var view: CatsTableViewControllerInterface
    let router: CatsTableRouterInterface?
    let interactor: CatsTableInteractorInterface?
    
    //MARK: - Private
    private var isLoading : Bool = false {
        didSet {
            tableDataSource.isLoading = isLoading
        }
    }
    
    private var currentPage : Int = 0
    
    //MARK: Data source
    private let tableDataSource: CatTableDataSource
    
    init(interactor: CatsTableInteractorInterface, router: CatsTableRouterInterface, view: CatsTableViewControllerInterface) {
        self.view = view
        self.interactor = interactor
        self.router = router
        
        tableDataSource = CatTableDataSource(isLoading: isLoading)
    }
    
    private func fetchAdditionalItems() {
        currentPage += 1
        fetchCats()
    }
    
    private func refreshItems() {
        tableDataSource.items = []
        currentPage = 0
        fetchCats()
    }
    
    private func fetchCats() {
        interactor?.fetchCats(limit: Constants.itemsLimit,
                              page: currentPage)
    }
    
    private func getCatsTableConfigurators(catsData: [Cat]) -> [CellConfigurator] {
        let catsConfigurators = catsData.map { cat -> CatImageTableViewConfigurator in
            let catModel = CatImageModel(catData: cat)
            return CatImageTableViewConfigurator(item: catModel, onAction: {[weak self] in
                self?.view.openDetails(catInfo: cat)
            })
        }
        
        return catsConfigurators
    }
}

extension CatsTablePresenter: CatsTableInteractorOutput {
    func onCatsLoaded(result: Result<[Cat], Error>) {
        switch result {
        case .success(let cats):
            
            guard !tableDataSource.items.isEmpty else {
                tableDataSource.items.append(contentsOf: getCatsTableConfigurators(catsData: cats))

                isLoading = false
                
                DispatchQueue.main.async {[weak self] in
                    guard let self = self else { return }
                    self.view.animateLoading(state: self.isLoading)
                    self.view.reloadTableView()
                }
                
                return
            }
            
            view.tableViewBeginUpdates()
            var indexPaths: [IndexPath] = []

            for row in (tableDataSource.items.count..<(tableDataSource.items.count + cats.count)) {
                   indexPaths.append(IndexPath(row: row, section: 0))
            }
            
            tableDataSource.items.append(contentsOf: getCatsTableConfigurators(catsData: cats))

            isLoading = false
            
            DispatchQueue.main.async {[weak self] in
                guard let self = self else { return }
                self.view.insertRows(at: indexPaths)
                self.view.tableViewEndUpdates()
            }
            
        case .failure(let error):
            print(error.localizedDescription)
        }
    }
}

extension CatsTablePresenter: CatsTablePresenterInterface {
    func viewIsReady() {
        isLoading = true
        view.animateLoading(state: isLoading)
        fetchCats()
    }
    
    func configure(tableView: UITableView) {
        tableView.delegate = tableDataSource
        tableView.dataSource = tableDataSource
        tableDataSource.delegate = self
    }
}

extension CatsTablePresenter: TableViewScrollPaginatable {
    
    func loadAdditionalInfo() {
        isLoading = true
        fetchAdditionalItems()
    }
}
