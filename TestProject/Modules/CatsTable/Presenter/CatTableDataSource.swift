//
//  CatTableDataSource.swift
//  TestProject
//
//  Created by Nikita Elizarov on 30.06.2020.
//

import UIKit
import SkeletonView

protocol TableViewScrollPaginatable {
    func loadAdditionalInfo()
}

final class CatTableDataSource: NSObject {
    private struct Constants {
        static let contentOffset: CGFloat = 200
        static let estimatedHeightForRow: CGFloat = 100
    }
    
    var isLoading: Bool
    var items: [CellConfigurator]
    var delegate: TableViewScrollPaginatable?
    
    init(isLoading: Bool) {
        self.isLoading = isLoading
        self.items = [CellConfigurator]()
    }
}

//MARK: - UITableViewDataSource
extension CatTableDataSource: SkeletonTableViewDataSource, UITableViewDelegate {
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "CatImageTableViewCell"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: item).reuseId, for: indexPath) as! CatImageTableViewCell
        
        item.configure(cell: cell)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard !isLoading else { return }
        let configurator = items[indexPath.row]
        configurator.onAction?()
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.estimatedHeightForRow
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetThreshold = (scrollView.contentOffset.y + scrollView.frame.size.height) + Constants.contentOffset > scrollView.contentSize.height
        
        if (offsetThreshold && !isLoading) {
            delegate?.loadAdditionalInfo()
        }
    }
    
    
}
