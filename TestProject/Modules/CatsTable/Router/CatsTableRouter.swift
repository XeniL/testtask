//
//  CatsTableRouter.swift
//  CIViperGenerator
//
//  Created by Nikita Elizarov on 30.06.2020.
//  Copyright © 2020 Nikita Elizarov. All rights reserved.
//

import Foundation
import UIKit

protocol CatsTableRouterInterface: class {
    func openCatDetails(catInfo: Cat)
}

class CatsTableRouter: NSObject {

    weak var viewController: CatsTableViewController!

    static func setupModule() -> CatsTableViewController? {
        
        let vc = CatsTableViewController.instantiateFromNib()
        
        let interactor = CatsTableInteractor()
        let router = CatsTableRouter()
        let presenter = CatsTablePresenter(interactor: interactor, router: router, view: vc)
        
        vc.presenter = presenter
        vc.router = router
        router.viewController = vc
        interactor.presenter = presenter
        interactor.catsNetworkingService = CatsNetworkingService()
        return vc
    }
}

extension CatsTableRouter: CatsTableRouterInterface {
    func openCatDetails(catInfo: Cat) {
        let module = CatDetailRouter.setupModule(catInfo: catInfo)
        viewController.navigationController?.pushViewController(module, animated: true)
    }
}

