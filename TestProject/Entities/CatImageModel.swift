//
//  CatImageModel.swift
//  TestProject
//
//  Created by Nikita Elizarov on 30.06.2020.
//

import Foundation

struct CatImageModel {
    var id: String
    var imageURL: String
    var imageSize: String
    
    init(catData: Cat) {
        self.id = catData.id
        self.imageURL = catData.url
        self.imageSize = "width: \(catData.width) height: \(catData.height)"
    }
}
