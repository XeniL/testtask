//
//  Cat.swift
//  TestProject
//
//  Created by Nikita Elizarov on 30.06.2020.
//

import Foundation

// MARK: - Cat
struct Cat: Codable {
    let id: String
    let url: String
    let width, height: Int
    let breeds: [Breed]? = []
    let category: [Category]?
    let subId: String?
    let createdAt: String?
    let originalFilename: String?
    
    enum CodingKeys: String, CodingKey {
        case id, url, width, height, category, breeds
        case originalFilename = "original_filename"
        case createdAt = "created_at"
        case subId = "sub_id"
    }
}

// MARK: - Breed
struct Breed: Codable {
    let weight: Weight
        let id, name, temperament, origin: String
        let countryCodes, countryCode, breedDescription, lifeSpan: String
        let indoor: Int
        let altNames: String
        let adaptability, affectionLevel, childFriendly, dogFriendly: Int
        let energyLevel, grooming, healthIssues, intelligence: Int
        let sheddingLevel, socialNeeds, strangerFriendly, vocalisation: Int
        let experimental, hairless, natural, rare: Int
        let rex, suppressedTail, shortLegs: Int
        let wikipediaURL: String
        let hypoallergenic: Int
        let cfaURL: String?
        let vetstreetURL: String?
        let vcahospitalsURL: String?
        let lap: Int?

    enum CodingKeys: String, CodingKey {
        case weight, id, name, temperament, origin
                case countryCodes = "country_codes"
                case countryCode = "country_code"
                case breedDescription = "description"
                case lifeSpan = "life_span"
                case indoor
                case altNames = "alt_names"
                case adaptability
                case affectionLevel = "affection_level"
                case childFriendly = "child_friendly"
                case dogFriendly = "dog_friendly"
                case energyLevel = "energy_level"
                case grooming
                case healthIssues = "health_issues"
                case intelligence
                case sheddingLevel = "shedding_level"
                case socialNeeds = "social_needs"
                case strangerFriendly = "stranger_friendly"
                case vocalisation, experimental, hairless, natural, rare, rex
                case suppressedTail = "suppressed_tail"
                case shortLegs = "short_legs"
                case wikipediaURL = "wikipedia_url"
                case hypoallergenic
                case cfaURL = "cfa_url"
                case vetstreetURL = "vetstreet_url"
                case vcahospitalsURL = "vcahospitals_url"
                case lap
    }
}

// MARK: - Weight
struct Weight: Codable {
    let metric, imperial: String
}

struct Category: Codable {
    let id: Int
    let name: String
}
